/***
 * @Author: 码上talk|RC
 * @Date: 2020-07-13 15:09:02
 * @LastEditTime: 2020-07-13 15:09:33
 * @LastEditors: 码上talk|RC
 * @Description: 
 * @FilePath: /tacomall-springboot/tacomall-mapper/src/main/java/cn/codingtalk/tacomallmapper/member/MemberCoupon.java
 * @Just do what I think it is right
 */
package cn.codingtalk.tacomallmapper.member;

import org.springframework.stereotype.Repository;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import cn.codingtalk.tacomallentity.member.MemberCoupon;

@Repository
public interface MemberCouponMapper extends BaseMapper<MemberCoupon>{
    
}
